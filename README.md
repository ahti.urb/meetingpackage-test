Simple RESTful API server that returns from a database all venues in the range of a given location and radius.
# Pre-reqiuisites / install
1. MySQL server v8.0 or newer
2. Node v14.17.6 or newer

# Usage
1. Set up MySQL server with a user account and an empty database
2. Initialise the MySQL database from SQL dump in `db_seed.sql`
3. Install all required dependencies: `npm install`
4. Copy `config/.env_sample` to `config/.env` and update the configuration there
5. Run the server: `npm run start`
6. Connect a web browser to `http://localhost:8081` (assuming you set the port to 8081 in `.env` file)

# Tests
Run all tests (both Unit and E2E) by invoking from terminal `npm run test` or `npx jest`.
Run just the Unit tests: `npx jest ../test/unit.test.js`.
Run just E2E tests: `npx jest ../test/server.test.js`

# Comments
- Chose MySQL as database engine, because it has built-in support for Geo-data. 
  I believe from performance perspective it is optimal to run the spacial lookup calculations where the data is to avoid overhead of moving the data.
- Found it is optimal to store the data as POINT objects rather than raw coordinate values - one conversion less in lookup. Afterall, it is always possible to extract the individual coordinates if needed.
- Since the application is very simple I tested it manually to begin with. Adding Unit tests as the next step.
- Implemented only GET support as the input data is short and POST was not explicitly required.
# TODO aka Next steps
To get this production ready I believe the following needs to be implemented:
- Automated Unit Tests and basic end-to-end tests. 
  - https://medium.com/@palrajesh/nodejs-restful-api-testing-with-jest-and-supertest-490d636fe71 
  - JEST? https://www.testim.io/blog/jest-testing-a-helpful-introductory-tutorial/
- Implement full CRUD (create,read,update,delete) API
- Logging to a file. https://www.npmjs.com/package/simple-node-logger
- Application telemetry for monitoring and analytics
- Containerized deployment

# References used
- https://enlear.academy/working-with-geo-coordinates-in-mysql-5451e54cddec
- https://www.tutorialspoint.com/nodejs/nodejs_restful_api.htm
- https://stackoverflow.com/questions/15965166/what-are-the-lengths-of-location-coordinates-latitude-and-longitude
- https://sammeechward.com/testing-an-express-app-with-supertest-and-jest/
