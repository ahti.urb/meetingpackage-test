import request from 'supertest'
import 'regenerator-runtime/runtime' //https://stackoverflow.com/questions/42535270/regeneratorruntime-is-not-defined-when-running-jest-test

import { app, con } from '../src/app.js'

describe('E2E tests: GET /venues', () => {

  afterAll(() => {
    console.log("closing database connection..")
    con.end()
    console.log("closed.")
  })

  describe("when passed all valid input parameters (happy flow)", () => {
    test('should return 200 and a JSON response', async () => {
      const res = await request(app)
        .get('/venues?latitude=60.1724392&longitude=24.9394797&radius=10')
        .send()
      expect(res.statusCode).toBe(200)
    })
  })
  
  describe("when passed invalid radius containing alphabetic characters", () => {
    test('should return 400', async () => {
      const res = await request(app)
        .get('/venues?longitude=12B4')
        .send()
      expect(res.statusCode).toBe(400)
    })
  }) 

  describe("when passed invalid latitude  containing alphabetic characters", () => {
    test('should return 400', async () => {
      const res = await request(app)
        .get('/venues?latitude=60.34AA45454')
        .send()
      expect(res.statusCode).toBe(400)
    })
  }) 
  
  describe("when passed invalid longitude  containing alphabetic characters", () => {
    test('should return 400', async () => {
      const res = await request(app)
        .get('/venues?longitude=-24.34AA45454')
        .send()
      expect(res.statusCode).toBe(400)
    })
  }) 

  describe("when passed invalid radius containing alphabetic characters", () => {
    test('should return 400', async () => {
      const res = await request(app)
        .get('/venues?longitude=12B4')
        .send()
      expect(res.statusCode).toBe(400)
    })
  }) 
})