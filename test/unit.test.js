import request from 'supertest'
import 'regenerator-runtime/runtime' //https://stackoverflow.com/questions/42535270/regeneratorruntime-is-not-defined-when-running-jest-test

import { validateInputParams, con } from '../src/app.js'

describe('Unit tests', () => {

    afterAll(() => {
        console.log("closing database connection..")
        con.end()
        console.log("closed.")
      })

    describe("Test validateInput() when passed all valid input parameters (happy flow)", () => {
    test('should return is_valid=true and report no invalid ort missing parameters in response object', () => {
        const req_mock = { query: { 
        latitude: 60.1724392, 
        longitude: 24.9394797, 
        radius: 10 }
        }
        const test_result = validateInputParams(req_mock)
        expect(test_result.is_valid).toBe(true)
        expect(Object.keys(test_result.invalid_params).length).toBe(0)
        expect(test_result.missing_params.length).toBe(0)
    })
    })
})
