import express from 'express'
import dotenv from 'dotenv'
import mysql from 'mysql'

//use the dotenv library for importing configuration and sensitive data from the host environment
dotenv.config({ path: 'config/.env' })

var app = express()

//init database connection
var con = mysql.createConnection({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWD,
  database: process.env.MYSQL_DATABASE
})
con.connect(function(err) {
  if (err) throw err
})

//we print this when user omits required input parameters
const usageString = `API reference. \n
        input parameters:\n
        longitude (decimal) = longitude coordinate of current location (-90.0 ... 90.0). Sample value: 24.9394797\n
        latitude (decimal) = latitude coordinate of current location (-180.0 ... 180.0). Sample value: 60.1724392\n
        radius (km) = search radius in kilometers\n
        \n
        Example: http://localhost:8081/venues?latitude=60.1724392&longitude=24.9394797&radius=10`

//validation functions for input data
function isLatitude(lat) {
    return isFinite(lat) && Math.abs(lat) <= 90;
  }

function isLongitude(lng) {
    return isFinite(lng) && Math.abs(lng) <= 180;
}

const validateInputParams = (req) => {
    let validated_params = {}
    let invalid_params = {}
    let missing_params = []
    let is_valid = false

    //validate "longitude"
    if (isLongitude(req.query.longitude)) validated_params['longitude'] = req.query.longitude //valid longitude given
    else if (!req.query.longitude) missing_params.push('longitude') //longitude not given
    else invalid_params['longitude'] = (req.query.longitude) ? req.query.longitude : '' //longitude value empty. Redefine to avoid JSON.stringify return empty object

    //validate "latitude"
    if (isLatitude(req.query.latitude)) validated_params['latitude'] = req.query.latitude //valid latitude given
    else if (!req.query.latitude) missing_params.push('latitude') //latitude not given
    else invalid_params['latitude'] = (req.query.latitude) ? req.query.latitude : '' //latitude value empty. Redefine to avoid JSON.stringify return empty object

    //validate "radius"
    if (isFinite(req.query.radius)) validated_params['radius'] = req.query.radius //valid radius given
    else if (!req.query.radius) missing_params.push('radius') //radius not given
    else invalid_params['radius'] = (req.query.radius) ? req.query.radius : '' //radius value empty. Redefine to avoid JSON.stringify return empty object
    
    if (!Object.keys(invalid_params).length && !missing_params.length) is_valid = true

    //if all validation successful
    return { is_valid, validated_params, invalid_params, missing_params }
}

app.get('/', function (req, res) {
    //automatically redirect requests against server root to the "venues" API endpoint, for convenience
    res.redirect('/venues') 
})

//API endpoint for /venues
app.get('/venues', function (req, res) {
    //print Usage string in case no input parameters given
    if (!Object.keys(req.query).length) {
        res.status(200).send('<pre>'+usageString+'</pre>')
        return
    }

    //Validate input parameters
    let validation_result = validateInputParams(req)
    if (!validation_result.is_valid) {
        let response_body = {
            status: 400, 
            result: 'Bad request: got invalid or missing input parameters.'
        }
        if (validation_result.missing_params.length) response_body['missing'] = JSON.stringify(validation_result.missing_params)
        if (Object.keys(validation_result.invalid_params).length) response_body['invalid'] = JSON.stringify(validation_result.invalid_params)

        //res.set('Content-Type', 'application/JSON')
        res.status(400).json(JSON.parse(JSON.stringify(response_body)))
        return null
    } 
    let params = validation_result.validated_params //extract valid parameters for convenience

    if (!params) return //in case of invalid values abort serving the request

    let sql = "SELECT name, city," +
        " convert(ST_Distance_Sphere(position," +
        " ST_GeomFromText('POINT(" + params.longitude + " " + params.latitude + ")', 4326)) / 1000, FLOAT) AS distance_km"+
        " FROM Venues"+
        " HAVING distance_km <= " + params.radius

    con.query(sql, function (err, result) {
        if (err) {
            console.log('ERROR: failed to query MySQL server!')
            res.status(500).send('Got a server internal error.')
        }
        res.json( JSON.parse(JSON.stringify(result)) ) //return pretty-printed JSON
    });
})

export { app, con, validateInputParams }